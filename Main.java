import java.util.Scanner;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		
		while (choice != 3) {
			// 1. show the menu
			showMenu();
			
			Square s5=new Square(choice, choice, choice); 
			Triangle t5=new Triangle(choice, choice, choice); 
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();
			
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			System.out.println();
			if(choice==1)
			{
				
				Shape s=new Triangle(3, 4, 5);
				ArrayList<Shape> ShapeList = new ArrayList<>();
				ShapeList.add(s);
		
			}
			if(choice==2)
			{
				Shape s1=new Square(0, 0, 10);
				ArrayList<Shape>ShapeList =new ArrayList<>();
				ShapeList.add(s1);
			}
			if(choice==3)
			{
				s5.calculateArea();
				t5.calculateArea();
			 s5.printInfo();
			 t5.printInfo();
			}
		}}
	
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}
