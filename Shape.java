

public class Shape {
	private double base=1,radius=1,height=1;
	Shape(double b,double r,double h)
	{
		this.base=b;
		this.height=h;
		this.radius=r;
	}
	public double getBase() {
		return base;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public void setBase(double base) {
		this.base = base;
	}
	
}
